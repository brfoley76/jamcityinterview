# Outline
## Major questions: 
I'm just going to focus on 2 of the most relevant areas I've worked in. So I'll be ignoring a lot of the genetics and NLP that I've done. 

Instead, I'm going to present topics in modelling _social behaviour_, with a focus on Bayesian methods; and _decision making and learning_, with a focus on Bayesian models of cognition. 

## Major tools: 
* Linear and Linear Mixed models  
    * Correction for multiple testing  
    * Model selection (AIC, BIC)  
* Nonlinear models  
    * nonparametric tests for significance  
* Simulation  
    * Bayesian methods  

# Table of Contents
[TOC]

# Systems
## Animals: Mostly Drosophila (fruit flies) 
###  Behaviour: Mating 
* females mate rarely, and actively resist males  
* they use multiple sexual signals  
* more mating for males, in an evolutionary sense, means success  
* males harrass females  

![Drosophila courtship](./images/dros_courtship.jpg)

### Behaviour: Aggression/territoriality. 
* measured as territorial guarding and defense  
* a proxy for willingness to defend  
* conflates desire and ability  

![Drosophila aggression](./images/dros_aggression.jpg)


### Behaviour: Aggregation
* Drosophila do all their social behaviours on food  
* females seem mostly to show up for egg laying  
* males show up for mating opportunities  
* they join and leave groups dynamically  

![Drosophila aggregation](./images/dros_banana.gif)

## Animals: Humans
* exhibit complex social interactions online  
* can engage in repeated cooperative endeavors  
* can freeload, or actively 'troll'  
* can reward and punish  
* develop reputations  

![Human online communities](./images/g2g_capstone.png)

### Behaviour: Volunteering
* asking and answering questions  
* cooperating on projects  
* giving and receiving thanks  

### Behaviour: Generating speech
* sentiment  
* spelling and text complexity  
* text length  

# Studies
Here are 3, most recent, of my published papers on animal behaviour, plus a data boot camp project on human online interactions. 

## Approximate Bayesian Computation fit of a complex group dynamic model.
We previously showed:

* [Genetics has a strong effect on aggression in flies](./papers/2008_sex_and_violence.pdf)  
* but the dynamics might be complicated  
* it looks like there might be different winning strategies in how to get a mate.  
    * aggressive winning, vs courtship winning?   
* [these 'strategies' can shape the structure of whole populations](./papers/2011_SNC.pdf)

![Experimental populations, where males and females can move around](./images/dros_assay.jpg)

To [investigate effects of density, genetics, and dynamics](./papers/2015_Bayesian_Modelling_of_Groups.pdf) I created a simulation:

* males and females had social preferences for each other  
* there were sex-specific joining and leaving rates  
    * we fixed one of these rates to 1, for scale  
    * there were 6 free parameters  
* flies left patches based on how much they 'liked' where they were  

![Simulation](./images/dros_simulation.png)

* 11 empirical summary statistics to calculate S and S'   
    * means and variance around means  
* aggregated by genotype and density (30 treatments)  

![Model details](./images/fly_bayes_01.png)

* the simulation was in C++  
* each treatment was simulated in a minimum of 20 chains  
* number of replicate experiments, sampling rate, identical between simulation and experiment  
* Euclidian distance for S' (fit)  
* used simulated annealing and flexible threshold to establish an efficient fixed epsilon  
* burn-in allowed, stationarity measured with Komolgoroff-Smirnoff  
* sampled to remove autocorrelation  

![Model results](./images/fly_bayes_02.png)

* There was a problem with identifiability  
    * bimodal  
    
![Twin peaks](./images/fly_bayes_identifiability.png)

* was able to pick one peak based on strong Bayes factor support across all replicates  
    * made biological sense  
    
![Biological sense](./images/fly_bayes_03.png)

## Foraging and reversal learning in flies
* Rescorla Wagner is the standard reinforcement learning model  
    * so-called 'delta'  
* learning proceeds with sigmoid curve  
* lhere is a rate parameter  
* 'reversal learning' usually proceeds faster than learning  
    * rate parameter changes  
* rarely/ever has change in rate been estimated directly (in flies especially)  
* [is there genetic variation in reversal learning rate?](./papers/2017_reversal-learning.pdf)  

![Reversal design](./images/rev_01.png)

* flies learned to avoid bitter food. 
* were faster in reversal 

![Raw results](./images/rev_02.png)

* measured preference as log relative presence on an option  
    * log((n_a + 1)/(n_b +1))
    * bayesian smoothing to avoid division by zero  
* smoothed the curves using LOESS regression  
    * estimated the smoothing parameter using information criteria  
    * midpoint estimated as the rate parameter  
    
![Example curves](./images/rev_03.png)
    
* effect of genotype  
* effect of reversal  
* no effect of interaction  

Validated the effects we measured with all kinds of permutations. These results are robust.

## Reasoning with uncertainty in animal behaviour

* animals forage probabilistically  
* usually learning/belief is modelled as point estimate   
* and a separate 'choice' parameter is added to explain behaviour  

![Bee foraging](./images/uncertainty_01.png)

* a big mostly unasked question in animal behaviour is what internal models underlie probabilistic behaviour
* the behaviour is often modelled with complicated, ad hoc, heuristics.
* probabilistic choice is easy to explain using probability distributions  
    * it is easy to explain with what we know of neurons  
    * it's basically Bayesian reasoning  
* Bayesian learning, and choice, has often been thought 'too hard' for animals  
* but it's [super simple to model with modified Rescorla Wagner](./papers/2017_Bayesian_reasoning_in_animals.pdf)  
    * that explicity includes an 'uncertainty' term  
    * variance around the belief  
* animals update continuously  
* and assuming animals sample from their posterior beliefs  

![Two models](./images/uncertainty_02.png)

* explains observed patterns of sampling with few additional degrees of freedom than standard models  
* and makes lots of cool predictions about animal behaviour  
* otherwise, we need a lot of heuristics to explain different patterns of choice  
* instead, maybe animals choose in order to reduce uncertainty  

![Two models redux](./images/uncertainty_03.png)

## Dimensions of human social behaviour in an online community

* there are many cooperative and selfish ways individuals in a community can interact  
* I chose a community that provides feedback and policing  
* can we tell which users do good, and help others do good?   
    * [the details for the analysis are in this repo](https://bitbucket.org/brfoley76/g2g_analysis/src/master/)  

![WikiTree Helpful](./images/g2g_01.png)

* got users on a genealogy website  
* looked at timelines and numbers of contributions  
* looked at up and downvotes  
* scraped a the qa for text  
* and created a bunch of measures of performance  

![WikiTree metrics](./images/g2g_02.png)

* tried to see how these metrics covaried. Found 3 major axes  

![WikiTree axes](./images/g2g_03.png)


    
